# vim-findroot for Pearl

Auto change directory to project root directory of the file.

## Details

- Plugin: https://github.com/mattn/vim-findroot
- Pearl: https://github.com/pearl-core/pearl
